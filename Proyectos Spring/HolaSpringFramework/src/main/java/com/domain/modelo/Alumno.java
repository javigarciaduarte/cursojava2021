package com.domain.modelo;

import java.util.List;

public class Alumno implements Modelo, Vaciable{
	private int id;
	private String nombre;
	private String apellido;
	private String estudios;
	private String link;
	private List<PracticaResuelta> practicasResueltas;
	
	public Alumno() {
		
	}

	public Alumno(String nombre, String apellido, String estudios, String link) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.estudios = estudios;
		this.link = link;
	}
	
	public Alumno(int id, String nombre, String apellido, String estudios, String link) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.estudios = estudios;
		this.link = link;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public List<PracticaResuelta> getPracticasResueltas() {
		return practicasResueltas;
	}

	public void setPracticasResueltas(List<PracticaResuelta> practicasResueltas) {
		this.practicasResueltas = practicasResueltas;
	}
	
	public boolean equals(Object obj) {
		return obj instanceof Alumno &&
				((Alumno) obj).getNombre().equals(nombre) &&
				((Alumno) obj).getApellido().equals(apellido);
	}
	
	public int hashCode() {
		return nombre.hashCode() + apellido.hashCode();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("Id: ");
		sb.append(this.id);
		sb.append(", Nombre: ");
		sb.append(this.nombre);
		sb.append(" ");
		sb.append(this.apellido);
		sb.append(" (");
		sb.append(this.link);
		sb.append(") estudi� ");
		sb.append(this.estudios);
		
		return sb.toString();
	}
	
	@Override
	public boolean isEmpty() {		
		return id == 0 && nombre == null &&	apellido == null && estudios == null && link == null && practicasResueltas == null;
	}
}
