package com.domain.modelo.dao.selectStrategy;

public class Codigo0Strategy extends SelectStrategy{

	public Codigo0Strategy() {		
	}

	@Override
	public String getCondicion() {
		StringBuilder sb = new StringBuilder(" where alu_id=");
		sb.append(alumno.getId());
		return sb.toString();
	}

	@Override
	public boolean isMe() {		
		isUltimo = alumno.getId()>0;
		return alumno.getId()>0;
	}

}
