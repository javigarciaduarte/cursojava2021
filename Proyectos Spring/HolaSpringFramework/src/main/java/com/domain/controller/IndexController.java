package com.domain.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.domain.modelo.Modelo;
import com.domain.modelo.dao.AlumnoDAO;

@Controller
public class IndexController {
	@RequestMapping("/home")
	public String goIndex() {
		return "index";
	}

	@RequestMapping("/")
	public String getPresentacion() {
		return "presentacion";
	}

	@RequestMapping("/listado")
	public String getListado(Model model) throws ClassNotFoundException, SQLException {
		List<Modelo> alumnos = null;
		
		AlumnoDAO aluDao = new AlumnoDAO();
		
		alumnos = aluDao.leer(null);

		model.addAttribute("titulo", "Listado de alumnos");
		model.addAttribute("profesor", "Gabriel Casas");
		model.addAttribute("alumnos", alumnos);
		return "listado";
	}
}
