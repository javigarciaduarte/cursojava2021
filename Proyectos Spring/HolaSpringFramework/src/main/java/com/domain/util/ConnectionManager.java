package com.domain.util;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.SQLException;


public class ConnectionManager {
	private static Connection con;

	public ConnectionManager() {

	}

	public static void conectar() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");

		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root", "");
	}

	public static void desConectar() throws SQLException {
		con.close();
	}

	public static Connection getConection() {
		return con;
	}

}