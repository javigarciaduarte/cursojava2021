package com.javiCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.javiCode.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer>{
	public Usuario findByNombre(String user);
}
