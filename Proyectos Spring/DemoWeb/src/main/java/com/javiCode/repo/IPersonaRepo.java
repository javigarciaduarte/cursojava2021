package com.javiCode.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.javiCode.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer>{

}
