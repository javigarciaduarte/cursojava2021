package com.javiCode.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.javiCode.model.Persona;
import com.javiCode.repo.IPersonaRepo;

@Controller
public class DemoController {

	@Autowired
	private IPersonaRepo repo;

	@GetMapping("/greeting")
	public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
		if (name.endsWith("World")) {
			repo.delete(new Persona(1, "Javier"));
			name = "JaviCode";
		}
		
		Persona per = new Persona(1, name);
		repo.save(per);

		model.addAttribute("name", name);

		return "greeting";
	}
}
