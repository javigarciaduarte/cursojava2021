package objetos;

public class CajaDeAhorro extends Cuenta {
	// atributos
	private float interes;

	// constructores
	public CajaDeAhorro() {
	}

	public CajaDeAhorro(int numero, float saldo, float interes) {
		super(numero, saldo);
		this.interes = interes;
	}

	// accesos
	public float getInteres() {
		return interes;
	}

	public void setInteres(float interes) {
		this.interes = interes;
	}

	public void debitar(float pMonto) {
		//no se puede debitar m�s de lo que tiene
		if (pMonto <= getSaldo()) {
			setSaldo(getSaldo() - pMonto);
		}
	}
}
