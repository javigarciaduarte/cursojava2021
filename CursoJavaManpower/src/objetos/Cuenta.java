package objetos;

public class Cuenta {
	// atributos
	private int numero;
	private float saldo;

	
	
	public Cuenta() {
		numero = 10;
		saldo = 1000;
	}

	public Cuenta(int numero, float saldo) {
		this.numero = numero;
		this.saldo = saldo;
	}

	// accesos p�blicos
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	// m�todos de negocio
	public void acreditar(float pMonto) {
		saldo += pMonto;
	}
	
	public void debitar(float pMonto) {
		saldo -= pMonto;
	}

	@Override
	public String toString() {
		return "Cuenta [numero=" + numero + ", saldo=" + saldo + "]";
	}
	
	
}
