package objetos;

public class CuentaTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cuenta c1 = new Cuenta();
		Cuenta c2 = new Cuenta(20, 2000);
		
		System.out.println("Se cre� la cuenta " + c1.toString());
		System.out.println("Su saldo es " + c1.getSaldo());

		System.out.println("\nSe cre� la cuenta " + c2.toString());
		System.out.println("Su saldo es " + c2.getSaldo());
		
		System.out.println("\nA la primera cuenta le voy a acreditar 100");
		c1.acreditar(100);
		System.out.println("El nuevo saldo de la primera cuenta es " + c1.getSaldo());
		
		System.out.println("\nA la primera cuenta le voy a debitar 80");
		c1.debitar(80);
		System.out.println("El nuevo saldo de la primera cuenta es " + c1.getSaldo());
	}

}
