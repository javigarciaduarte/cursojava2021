package modulo6.util;

public class StringUtil {

	private static int espacios = 0;
	private static boolean r = false, numero = false;
	private static String str = "", str2 = "";

	public static void main(String[] args) {
		str = "asd qwe rty";
		str2 = "1234";
		
		System.out.println(containsDobleSpace(str2));
		System.out.println(containsNumber(str));
	}

	public static boolean containsDobleSpace(String str) {
		for (int i = 0; i < str.length(); i++) {
			System.out.println(str.charAt(i));

			if (str.charAt(i) == ' ') {
				espacios++;
			}
		}

		if (espacios >= 2) {
			r = true;
		} else {
			r = false;
		}

		return r;

	}

	public static boolean containsNumber(String str) {
		for (int i = 0; i < str.length(); i++) {
			if (Character.isDigit(str.charAt(i))) {
				numero = true;
			}
		}

		return numero;
	}
}
