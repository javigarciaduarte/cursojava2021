package modulo6.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	private static Calendar cal = Calendar.getInstance();

	public static void main(String[] args) throws ParseException {
		cal.set(200, Calendar.AUGUST, 21);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yyyy");
		Date d = sdf.parse("21-08-2000");
		
		System.out.println(getAnno(d));
		System.out.println(getMes(d));
		System.out.println(getDia(d));
		System.out.println(isFinDeSemana(d));
		System.out.println(isDiaDeSemana(d));
		System.out.println(getDiaDeSemana(d));
		System.out.println(asDate("dd-mm-yyyy", "21-08-2000"));
		System.out.println(asCalendar("dd-mm-yyyy", "21-08-2000"));
		System.out.println(asString("dd-mm-yyyy", d));
	}

	public static int getAnno(Date fecha) {
		cal.setTime(fecha);
		return cal.get(Calendar.YEAR);
	}

	public static int getMes(Date fecha) {
		cal.setTime(fecha);
		return cal.get(Calendar.MONTH);
	}

	public static int getDia(Date fecha) {
		cal.setTime(fecha);
		return cal.get(Calendar.DAY_OF_MONTH);
	}

	public static boolean isFinDeSemana(Date fecha) {
		boolean wk = false;

		cal.setTime(fecha);
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			wk = true;
		}

		return wk;
	}

	public static boolean isDiaDeSemana(Date fecha) {
		boolean wk = true;

		cal.setTime(fecha);
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			wk = false;
		}
		
		return wk;
	}

	public static int getDiaDeSemana(Date fecha) {
		cal.setTime(fecha);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	public static Date asDate(String pattern, String fecha) throws ParseException {
		Date date = new SimpleDateFormat(pattern).parse(fecha);
		return date;
	}

	public static Calendar asCalendar(String pattern, String fecha) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
		cal.setTime(sdf.parse(fecha));
		return cal;
	}

	public static String asString(String pattern, Date fecha) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(fecha);
	}
}
