package modulo3;

import java.util.Scanner;

public class Ejercicio8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el primer n�mero:");
		int n1 = sc.nextInt();
		
		System.out.println("Introduce el segundo n�mero:");
		int n2 = sc.nextInt();
		
		if (n1 == 0) {
			if (n2 == 0) {
				System.out.println("Empate");
			} else if (n2 == 1) {
				System.out.println("Gana el segundo");
			} else if (n2 == 2) {
				System.out.println("Gana el primero");
			}
		} else if (n1 == 1) {
			if (n2 == 0) {
				System.out.println("Gana el primero");
			} else if (n2 == 1) {
				System.out.println("Empate");
			} else if (n2 == 2) {
				System.out.println("Gana el segundo");
			}
		} else if (n1 == 2) {
			if (n2 == 0) {
				System.out.println("Gana el segundo");
			} else if (n2 == 1) {
				System.out.println("Gana el primero");
			} else if (n2 == 2) {
				System.out.println("Empate");
			}
		} else {
			System.out.println("Error");
		}
	}

}
