package modulo3;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Introduce la categor�a del coche:");
		Scanner sc = new Scanner(System.in);
		String n1 = sc.next();
		
		char c1 = n1.charAt(0);
		
		switch (c1) {
		case 'a':
			System.out.println("\n4 ruedas\nMotor");
			break;
		case 'b':
			System.out.println("\n4 ruedas\nMotor\nCierre centralizado\nAire acondicionado");
			break;
		case 'c':
			System.out.println("\n4 ruedas\nMotor\nCierre centralizado\nAire acondicionado\nAirbag");
			break;
		default:
			System.out.println("Categor�a no v�lida");
			break;
		}
	}

}
