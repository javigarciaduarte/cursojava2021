package modulo3;

public class Ejercicio20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int contador = 0, n = 0, max = 0, min = 100;
		
		while (contador != 10) {
			contador++;
			n += (int) (Math.random() * 25);
			
			System.out.println(n);
			
			if (n > max) {
				max = n;
			}
			
			if (n < min) {
				min = n;
			}
		}
		
		System.out.println("El mayor es " + max);
		System.out.println("El menor es " + min);
	}

}
