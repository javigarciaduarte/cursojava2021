package modulo3;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Introduce la categor�a:");
		Scanner sc = new Scanner(System.in);
		String st = sc.next();
		st.toUpperCase();
		char categoria = st.charAt(0);

		System.out.println("Introduce la antig�edad:");
		int antiguedad = sc.nextInt();

		System.out.println("Introduce el sueldo:");
		float sueldo = sc.nextFloat();

		if (antiguedad >= 1 && antiguedad <= 5) {
			sueldo *= 1.05f;
		} else if (antiguedad >= 6 && antiguedad <= 10) {
			sueldo *= 1.1f;
		} else if (antiguedad > 10) {
			sueldo *= 1.3f;
		}

		switch (categoria) {
		case 'A':
			sueldo += 1000;
			System.out.println("EL sueldo neto es de " + sueldo + "�");
			break;
		case 'B':
			sueldo += 2000;
			System.out.println("EL sueldo neto es de " + sueldo + "�");
			break;
		case 'C':
			sueldo += 3000;
			System.out.println("EL sueldo neto es de " + sueldo + "�");
			break;
		default:
			System.out.println("Error");
			break;
		}
	}

}
