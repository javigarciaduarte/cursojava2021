package modulo3;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el caracter:");
		String n1 = sc.next();
		
		char c1 = n1.charAt(0);
		
		if (c1 == 'a' || c1 == 'e' || c1 == 'i' || c1 == 'o' || c1 == 'u') {
			System.out.println("Es una vocal");
		} else {
			System.out.println("No es una vocal");
		}
	}

}
