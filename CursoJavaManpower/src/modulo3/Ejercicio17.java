package modulo3;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Introduce un n�mero:");
		Scanner sc = new Scanner(System.in);
		int n1 = sc.nextInt();
		int suma = 0;
		
		//Con if
		for (int i = 0; i < 11; i++) {
			if(i % 2 == 0) {
				System.out.println(n1 + " * " + i + " = " + n1*i);
				suma += n1 * i;
			}
		}
		
		System.out.println("La suma de los pares es " + suma + "\n");
		
		//Sin if
		suma = 0;
		for (int i = 0; i < 11; i+=2) {
			System.out.println(n1 + " * " + i + " = " + n1*i);
			suma += n1 * i;
		}
		System.out.println("La suma de los pares es " + suma);
	}

}
