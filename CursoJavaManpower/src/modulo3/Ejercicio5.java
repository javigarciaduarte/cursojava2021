package modulo3;

import java.util.Scanner;

public class Ejercicio5 {
	
	public static void main(String[] args) {
		System.out.println("Introduce tu posici�n:");
		Scanner sc = new Scanner(System.in);
		int n1 = sc.nextInt();
		
		if(n1 == 1) {
			System.out.println("Medalla de oro");
		}

		if(n1 == 2) {
			System.out.println("Medalla de plata");
		}
		
		if(n1 == 3) {
			System.out.println("Medalla de bronce");
		}
		
		if(n1 > 3) {
			System.out.println("Siga participando");
		}
		
		if(n1 < 1) {
			System.out.println("Posici�n no v�lida");
		}
	}
}
