package modulo3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Introduce un mes (n�mero):");
		Scanner sc = new Scanner(System.in);
		int n1 = sc.nextInt();
		
		if(n1 == 1) {
			System.out.println("Enero, 31 d�as");
		}

		if(n1 == 2) {
			System.out.println("Febrero, 28 d�as");
		}
		
		if(n1 == 3) {
			System.out.println("Marzo, 31 d�as");
		}
		
		if(n1 == 4) {
			System.out.println("Abril, 30 d�as");
		}

		if(n1 == 5) {
			System.out.println("Mayo, 31 d�as");
		}
		
		if(n1 == 6) {
			System.out.println("Junio, 30 d�as");
		}
		
		if(n1 == 7) {
			System.out.println("Julio, 31 d�as");
		}

		if(n1 == 8) {
			System.out.println("Agosto, 31 d�as");
		}
		
		if(n1 == 9) {
			System.out.println("Septiembre, 30 d�as");
		}
		
		if(n1 == 10) {
			System.out.println("Octubre, 31 d�as");
		}

		if(n1 == 11) {
			System.out.println("Noviembre, 30 d�as");
		}
		
		if(n1 == 12) {
			System.out.println("Diciembre, 31 d�as");
		}
	}

}
