package modulo3;

public class Ejercicio19 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int contador = 0;
		int suma = 0;
		
		while (contador != 10) {
			suma += (int) (Math.random() * 10);
			contador++;
		}
		
		System.out.println("Suma: " + suma);
		System.out.println("Promedio: " + suma/10f);
	}

}
