package modulo3;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Ejercicio22 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int contador = 0;
		DecimalFormat df = new DecimalFormat("###.##");
		
		while (contador != 10) {
			contador++;
			
			int antiguedad = (int) (Math.random()*20+1);
			float sueldo = (float) Math.random()*50000+5000;
			int cat = (int) (Math.random()*3+1);
			char categoria = 'Q';
			
			switch (cat) {
			case 1:
				categoria = 'A';
				break;
			case 2:
				categoria = 'B';
				break;
			case 3:
				categoria = 'C';
				break;
			}
			
			System.out.println("Categor�a: " + categoria + "\nAntig�edad: " + antiguedad + " a�os\nSueldo bruto: " + df.format(sueldo) + "�");
			
			if (antiguedad >= 1 && antiguedad <= 5) {
				sueldo *= 1.05f;
			} else if (antiguedad >= 6 && antiguedad <= 10) {
				sueldo *= 1.1f;
			} else if (antiguedad > 10) {
				sueldo *= 1.3f;
			}

			switch (categoria) {
			case 'A':
				sueldo += 1000;
				System.out.println("EL sueldo neto es de " + df.format(sueldo) + "�\n");
				break;
			case 'B':
				sueldo += 2000;
				System.out.println("EL sueldo neto es de " + df.format(sueldo) + "�\n");
				break;
			case 'C':
				sueldo += 3000;
				System.out.println("EL sueldo neto es de " + df.format(sueldo) + "�\n");
				break;
			default:
				System.out.println("Error");
				break;
			}
		}
	}

}
