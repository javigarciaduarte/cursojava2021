package modulo3;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduce el n�mero:");
		int n1 = sc.nextInt();
		
		if(n1 < 1) {
			System.out.println("El n�mero " + n1 + " est� fuera de rango");
		} else if(n1 <= 12 && n1 >= 1) {
			System.out.println("El n�mero " + n1 + " est� en la primera docena");
		} else if(n1 <= 24 && n1 >= 13) {
			System.out.println("El n�mero " + n1 + " est� en la segunda docena");
		} else if(n1 <= 36 && n1 >= 25) {
			System.out.println("El n�mero " + n1 + " est� en la tercera docena");
		} else {
			System.out.println("El n�mero " + n1 + " est� fuera de rango");
		}
	}

}
