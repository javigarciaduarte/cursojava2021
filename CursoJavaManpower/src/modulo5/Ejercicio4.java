package modulo5;

public class Ejercicio4 {

	public static void main(String[] args) {
		String str = "esto es una prueba de la clase String";
		int v = 0, c = 0;
		
		str = str.toLowerCase();
		
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u') {
				v++;
			} else if(str.charAt(i) != ' ') {
				c++;
			}
		}
		
		System.out.println("Vocales = " + v);
		System.out.println("Consonantes = " + c);
	}
}
