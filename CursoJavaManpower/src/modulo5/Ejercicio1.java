package modulo5;

public class Ejercicio1 {
	
	public static void main(String[] args) {
		String str = "HeLLo WorLD";
		
		System.out.println(str.toLowerCase());
		System.out.println(str.toUpperCase());
		System.out.println(str.replace('o', '2'));
	}
}
