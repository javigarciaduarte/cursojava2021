package modulo5;

public class Ejercicio5 {
	
	public static void main(String[] args) {
		String str = "gcasas1972@gmail.com";
		String str1, str2;
		//El conteo empieza en 0
		System.out.println("Posición de @ = " + str.indexOf('@'));
		
		//Si queremos que empiece en 1 sumamos 1 a la operación
		System.out.println("Posición de @ = " + (str.indexOf('@') + 1));
		
		str1 = str.substring(0, str.indexOf('@'));
		str2 = str.substring(str.indexOf('@') + 1, str.length());
		
		System.out.println(str1);
		System.out.println(str2);
	}
}
