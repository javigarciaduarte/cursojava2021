package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte bmin = -128;
		byte bmax = 127;
		short smin = (short) (Math.pow(2, 15) * -1);
		short smax = (short) (Math.pow(2, 15)-1);
		int imin = (int) (Math.pow(2, 31) * -1);
		int imax = (int) (Math.pow(2, 31)-1);
		long lmin = (long) (Math.pow(2, 63) * -1);
		long lmax = (long) (Math.pow(2, 63)-1);
		
		System.out.println("tipo\tminimo\t\t\tmaximo");
		System.out.println("....\t......\t\t\t......");
		System.out.println("\nbyte\t" + bmin + "\t\t\t" + bmax);
		System.out.println("\nshort\t" + smin + "\t\t\t" + smax);
		System.out.println("\nint\t" + imin + "\t\t" + imax);
		System.out.println("\nlong\t" + lmin + "\t" + lmax);
		
		//La f�rmula que nos permite hallar los m�ximos es 2^(n-1), siendo n la cantidad de bits
		//La f�rmula que nos permite hallar los m�nimos es 2^(n-1)-1, siendo n la cantidad de bits
	}

}
