package modulo2;

public class Ejercicio1 {

	public static void main(String[] args) {
		byte bmin = -128;
		// byte -> short -> int-> float -> double (autoCast)
		// int -> short (necesita casteo)
		/*
		 *  64  8
		 * 010 01101
		 *       4 1
		 */
		
		short s = bmin;
		int i = 1357;
		//necesita casteo, yo me hago responsable
		
		//cuentas
		float nro1 = 0;
		int nro2 = 3;
		float f = nro1/nro1;
		double raiz = Math.sqrt(-1);
		byte b = (byte) i;
		
		System.out.println("Raiz = " + raiz);		
		System.out.println("f = " + f);		
		System.out.println("bmin = " + bmin);
		System.out.println("i = " + i);
		System.out.println("b = " + b);
	}

}
