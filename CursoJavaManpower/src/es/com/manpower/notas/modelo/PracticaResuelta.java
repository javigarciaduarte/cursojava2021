package es.com.manpower.notas.modelo;

public class PracticaResuelta {
	private int id;
	private float nota;
	private String observaciones;
	
	public PracticaResuelta() {
		
	}

	public PracticaResuelta(int id, float nota, String observaciones) {
		super();
		this.id = id;
		this.nota = nota;
		this.observaciones = observaciones;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	//TODO equals, hashCode y toString
	public boolean equals(Object obj) {
		return obj instanceof PracticaResuelta &&
				((PracticaResuelta) obj).getObservaciones().equals(observaciones) &&
				((PracticaResuelta) obj).getNota() == nota;
	}
	
	public int hashCode() {
		return observaciones.hashCode();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("Id: ");
		sb.append(this.id);
		sb.append(", Nota: ");
		sb.append(this.nota);
		sb.append("\n");
		sb.append(this.observaciones);
		
		return sb.toString();
	}
}
