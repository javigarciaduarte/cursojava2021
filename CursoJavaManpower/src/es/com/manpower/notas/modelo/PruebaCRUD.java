package es.com.manpower.notas.modelo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import es.com.manpower.notas.modelo.dao.AlumnoDAO;

public class PruebaCRUD {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		AlumnoDAO adao = new AlumnoDAO();
		
		Alumno a1 = new Alumno("kk", "kk", "kk", "kk");
		Alumno a2 = new Alumno("gg", "gg", "gg", "gg");
		Alumno a3 = new Alumno("ff", "ff", "ff", "ff");
		Alumno a4 = new Alumno("dd", "dd", "dd", "dd");
		Alumno a5 = new Alumno("ss", "ss", "ss", "ss");
		
		adao.anadir(a1);
		adao.anadir(a2);
		adao.anadir(a3);
		adao.anadir(a4);
		adao.anadir(a5);
		
		System.out.println();
		adao.eliminar(a1);
		
		System.out.println();
		a5.setApellido("gg");
		adao.modificar(a5);
		
		System.out.println();
		List<Modelo> lista = new ArrayList<Modelo>();
		lista = adao.leerTodos();
		
		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i).toString());
		}
	}

}
