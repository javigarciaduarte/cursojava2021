package es.com.manpower.notas.modelo.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import es.com.manpower.notas.modelo.Alumno;
import es.com.manpower.notas.modelo.dao.AlumnoDAO;
import es.com.manpower.notas.modelo.dao.DAO;
import es.com.manpower.notas.util.ConnectionManager;

public class testAlumnoDAO {
	DAO alumnoDao;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager.connect();
		Connection con = ConnectionManager.getConnection();

		Statement consulta = con.createStatement();

		String sql = "";
		BufferedReader bf = new BufferedReader(
				new InputStreamReader(testAlumnoDAO.class.getResource("AlumnosCrear.sql").openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql);
			}
		}
		ConnectionManager.disconnect();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

		ConnectionManager.connect();
		Connection con = ConnectionManager.getConnection();

		Statement consulta = con.createStatement();

		String sql = "";
		BufferedReader bf = new BufferedReader(
				new InputStreamReader(testAlumnoDAO.class.getResource("AlumnosEliminar.sql").openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql);
			}
		}
		ConnectionManager.disconnect();
	}

	@Before
	public void setUp() throws Exception {
		alumnoDao = new AlumnoDAO();
	}

	@After
	public void tearDown() throws Exception {
		alumnoDao = null;
	}

	@Test
	public void testAgregar() {
		try {
			alumnoDao.anadir(new Alumno(0, "Gabriel_test", "Casas_test", "Estudios_test", "Repo_test"));
			// tengo que leer
			ConnectionManager.connect();
			Connection con = ConnectionManager.getConnection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select alu_nombre from alumnos where alu_nombre ='Gabriel_test'");
			rs.next();
			assertEquals("Gabriel_test", rs.getString("alu_nombre"));

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testModificar() {
		try {
			ConnectionManager.connect();
			Connection con = ConnectionManager.getConnection();
			Statement stm = con.createStatement();
			
			ResultSet rst = stm.executeQuery("Select ALU_ID from alumnos where alu_nombre = 'Javier'");
			rst.next();
			Alumno al = new Alumno(rst.getInt(1), "Javier_test", "Garc�a-Duarte S�enz_test", "DAM_test", "https://gitlab.com/javigarciaduarte/cursojava2021.git_test");
			alumnoDao.modificar(al);
			
			ResultSet rs = stm.executeQuery("Select * from alumnos where alu_nombre ='Javier_test'");
			rs.next();
			assertEquals("Javier_test", rs.getString("alu_nombre"));
			//assertEquals("Garc�a-Duarte S�enz_test", rs.getString("alu_apellido"));
			//assertEquals("DAM_test", rs.getString("alu_estudios"));
			//assertEquals("https://gitlab.com/javigarciaduarte/cursojava2021.git_test", rs.getString("alu_linkgit"));

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testEliminar() {
		try {
			alumnoDao.eliminar(new Alumno(74, "Javier_test", "Garc�a-Duarte S�enz", "Grado superior DAM", "https://gitlab.com/javigarciaduarte/cursojava2021.git"));
			// tengo que leer
			ConnectionManager.connect();
			Connection con = ConnectionManager.getConnection();
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("Select alu_nombre from alumnos where alu_id = 74");
			assertFalse(rs.next());

		} catch (ClassNotFoundException | SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
/*
	@Test
	public void testLeer() {
		fail("Not yet implemented");
	}*/
}
