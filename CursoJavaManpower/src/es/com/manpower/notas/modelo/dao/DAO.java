package es.com.manpower.notas.modelo.dao;

import java.sql.SQLException;
import java.util.List;

import es.com.manpower.notas.modelo.Modelo;

public interface DAO {
	
	public void anadir(Modelo pMod) throws ClassNotFoundException, SQLException;
	public void modificar(Modelo pMod) throws ClassNotFoundException, SQLException;
	public List<Modelo> leerTodos() throws ClassNotFoundException, SQLException;
	public List<Modelo> leerUnico(Modelo pMod) throws ClassNotFoundException, SQLException;
	public void eliminar(Modelo pMod) throws ClassNotFoundException, SQLException;
}
