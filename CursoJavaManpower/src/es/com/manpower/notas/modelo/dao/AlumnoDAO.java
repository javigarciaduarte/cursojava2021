package es.com.manpower.notas.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import es.com.manpower.notas.modelo.Alumno;
import es.com.manpower.notas.modelo.Modelo;
import es.com.manpower.notas.util.ConnectionManager;

public class AlumnoDAO implements DAO {
	/*
	 * Archivo de pruebas PruebaCRUD
	 * Falta testeo de m�todos
	 */
	
	private Connection con;
	
	public AlumnoDAO() {

	}

	@Override
	public void anadir(Modelo pMod) throws ClassNotFoundException, SQLException {
		Alumno a = (Alumno) pMod;

		// Conexi�n a la BBDD
		ConnectionManager.connect();
		con = ConnectionManager.getConnection();

		// Query
		PreparedStatement ps = con.prepareStatement(
				"INSERT INTO alumnos(ALU_NOMBRE, ALU_APELLIDO, ALU_ESTUDIOS, ALU_LINKGIT) VALUES (?, ?, ?, ?)");
		ps.setString(1, a.getNombre());
		ps.setString(2, a.getApellido());
		ps.setString(3, a.getEstudios());
		ps.setString(4, a.getLink());

		// Ejecuci�n
		ps.executeUpdate();

		// Cerrar conexi�n
		ConnectionManager.disconnect();
		System.out.println("A�adido correctamente " + a.toString());
	}

	@Override
	public void modificar(Modelo pMod) throws ClassNotFoundException, SQLException {
Alumno alumno= (Alumno)pMod;
		
		//1- me tengo que conectar
		ConnectionManager.connect();
		con = ConnectionManager.getConnection();
		//2- statemente
		
		StringBuilder sql = new StringBuilder("update alumnos");
		sql.append(" set ALU_NOMBRE=? ,ALU_APELLIDO=?,ALU_ESTUDIOS=?,ALU_LINKGIT=?");
		sql.append(" where alu_id=?");
		
		PreparedStatement pstm = con.prepareStatement(sql.toString());
		pstm.setString(1, alumno.getNombre());
		pstm.setString(2, alumno.getApellido());
		pstm.setString(3, alumno.getEstudios());
		pstm.setString(4, alumno.getLink());
		pstm.setInt(5, alumno.getId());
		
		pstm.executeUpdate();				
		ConnectionManager.disconnect();
		System.out.println("Modificado correctamente " + alumno.toString());
	}

	@Override
	public List<Modelo> leerTodos() throws ClassNotFoundException, SQLException {
		Alumno a = new Alumno();
		List<Modelo> lista = new ArrayList<Modelo>();
		
		// Conexi�n a la BBDD
		ConnectionManager.connect();
		con = ConnectionManager.getConnection();

		// Query
		PreparedStatement ps = con.prepareStatement(
				"SELECT * FROM alumnos");

		// Ejecuci�n
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			a.setId(rs.getInt("ALU_ID"));
			a.setNombre(rs.getString("ALU_NOMBRE"));
			a.setApellido(rs.getString("ALU_APELLIDO"));
			a.setEstudios(rs.getString("ALU_ESTUDIOS"));
			a.setLink(rs.getString("ALU_LINKGIT"));
			
			lista.add(a);
			a = new Alumno();
		}

		// Cerrar conexi�n
		ConnectionManager.disconnect();
		return lista;
	}
	
	@Override
	public List<Modelo> leerUnico(Modelo pMod) throws ClassNotFoundException, SQLException {
		Alumno a = (Alumno) pMod;
		List<Modelo> lista = new ArrayList<Modelo>();
		
		// Conexi�n a la BBDD
		ConnectionManager.connect();
		con = ConnectionManager.getConnection();

		// Query
		PreparedStatement ps = con.prepareStatement(
				"SELECT * FROM alumnos WHERE ALU_ID=?");
		ps.setInt(1, a.getId());

		// Ejecuci�n
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			a.setId(rs.getInt("ALU_ID"));
			a.setNombre(rs.getString("ALU_NOMBRE"));
			a.setApellido(rs.getString("ALU_APELLIDO"));
			a.setEstudios(rs.getString("ALU_ESTUDIOS"));
			a.setLink(rs.getString("ALU_LINKGIT"));
			
			lista.add(a);
			a = new Alumno();
		}

		// Cerrar conexi�n
		ConnectionManager.disconnect();
		return lista;
	}

	@Override
	public void eliminar(Modelo pMod) throws ClassNotFoundException, SQLException {
		Alumno a = (Alumno) pMod;

		// Conexi�n a la BBDD
		ConnectionManager.connect();
		con = ConnectionManager.getConnection();

		// Query
		PreparedStatement ps = con.prepareStatement(
				"DELETE FROM alumnos WHERE ALU_ID=?");
		
		ps.setInt(1, a.getId());

		// Ejecuci�n
		ps.executeUpdate();

		// Cerrar conexi�n
		ConnectionManager.disconnect();
		System.out.println("Eliminado correctamente " + a.toString());
	}

}
