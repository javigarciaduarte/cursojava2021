package es.com.manpower.notas.util.test;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.com.manpower.notas.util.ConnectionManager;

class ConnectionManagerTest {
	Connection con;
	
	@BeforeEach
	public void setUp() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");

		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower", "root", "");
		ConnectionManager.connect();
	}
	
	@After
	public void tearDown() throws SQLException {
		con = null;
		ConnectionManager.disconnect();
	}

	@Test
	void testConnect() throws ClassNotFoundException, SQLException {
		ConnectionManager.connect();
		assertTrue(ConnectionManager.getConnection().isValid(1));
	}

	@Test
	void testDisconnect() throws SQLException, ClassNotFoundException {
		//ConnectionManager.connect();
		ConnectionManager.disconnect();
		assertTrue(ConnectionManager.getConnection().isClosed());
	}

	@Test
	void testGetConnection() throws SQLException {
		assertFalse(ConnectionManager.getConnection().isClosed());
	}

}
