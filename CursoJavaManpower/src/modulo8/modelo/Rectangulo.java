package modulo8.modelo;

import java.util.Objects;

public class Rectangulo extends Figura {

	private float altura;
	private float base;
	
	public Rectangulo() {
		// TODO Auto-generated constructor stub
	}

	public Rectangulo(String nombre, float altura, float base) {
		super(nombre);
		this.altura = altura;
		this.base = base;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	public float getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	@Override
	public String toString() {
		return "Rectangulo [altura=" + altura + ", base=" + base + super.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(altura, base);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rectangulo other = (Rectangulo) obj;
		return Float.floatToIntBits(altura) == Float.floatToIntBits(other.altura)
				&& Float.floatToIntBits(base) == Float.floatToIntBits(other.base);
	}
	
	@Override
	public float calcularPerimetro() {
		return (2 * base) + (2 * altura);
	}

	@Override
	public float calcularSuperficie() {
		float superficie = base * altura;
		
		if (superficie > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = superficie;
		}
		
		return superficie;
	}

	@Override
	public String getValores() {
		return super.getValores();
	}
}
