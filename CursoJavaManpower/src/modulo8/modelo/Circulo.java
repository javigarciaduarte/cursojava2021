package modulo8.modelo;

import java.util.Objects;

public class Circulo extends Figura {

	private float radio;
	public Circulo() {
		// TODO Auto-generated constructor stub
	}

	public Circulo(String nombre, float radio) {
		super(nombre);
		this.radio = radio;
	}

	public float getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(radio);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Circulo other = (Circulo) obj;
		return Float.floatToIntBits(radio) == Float.floatToIntBits(other.radio);
	}

	@Override
	public String toString() {
		return "Circulo [radio=" + radio + super.toString() + "]";
	}

	@Override
	public float calcularPerimetro() {
		return (float) (2 * Math.PI * radio);
	}

	@Override
	public float calcularSuperficie() {
		float superficie = (float) (Math.PI * Math.pow(radio, 2));
		
		if (superficie > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = superficie;
		}
		
		return superficie;
	}

	@Override
	public String getValores() {
		return super.getValores();
	}
}
