package modulo8.modelo;

import java.util.Objects;

public class Triangulo extends Figura {
	
	private float lado1;
	private float lado2;
	private float lado3;
	
	public Triangulo() {
		// TODO Auto-generated constructor stub
	}

	public Triangulo(String nombre, float lado1, float lado2, float lado3) {
		super(nombre);
		this.lado1 = lado1;
		this.lado2 = lado2;
		this.lado3 = lado3;
	}

	public float getLado1() {
		return lado1;
	}

	public void setLado1(float lado1) {
		this.lado1 = lado1;
	}

	public float getLado2() {
		return lado2;
	}

	public void setLado2(float lado2) {
		this.lado2 = lado2;
	}

	public float getLado3() {
		return lado3;
	}

	public void setLado3(float lado3) {
		this.lado3 = lado3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(lado1, lado2, lado3);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Triangulo other = (Triangulo) obj;
		return Float.floatToIntBits(lado1) == Float.floatToIntBits(other.lado1)
				&& Float.floatToIntBits(lado2) == Float.floatToIntBits(other.lado2)
				&& Float.floatToIntBits(lado3) == Float.floatToIntBits(other.lado3);
	}

	@Override
	public String toString() {
		return "Triangulo [lado1=" + lado1 + ", lado2=" + lado2 + ", lado3=" + lado3 + super.toString() + "]";
	}
	
	@Override
	public float calcularPerimetro() {
		return lado1 + lado2 + lado3;
	}

	@Override
	public float calcularSuperficie() {
		float s = calcularPerimetro()/2;
		float superficie = (float) (Math.sqrt(s * (s - lado1) * (s - lado2) * (s - lado3)));
		
		if (superficie > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = superficie;
		}
		
		return superficie;
	}

	@Override
	public String getValores() {
		return super.getValores();
	}
}
