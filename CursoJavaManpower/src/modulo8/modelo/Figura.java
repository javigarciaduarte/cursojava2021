package modulo8.modelo;

import java.util.Objects;

public abstract class Figura {

	protected static float maximaSuperficie;
	private String nombre;
	
	public Figura() {
		super();
	}
	
	public Figura(String nombre) {
		super();
		this.nombre = nombre;
	}

	public static float getMaximaSuperficie() {
		return maximaSuperficie;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Figura other = (Figura) obj;
		return Objects.equals(nombre, other.nombre);
	}

	@Override
	public String toString() {
		return "Figura [nombre=" + nombre + "]";
	}
	
	public abstract float calcularPerimetro();
	
	public abstract float calcularSuperficie();
	
	public String getValores(){
		StringBuilder str = new StringBuilder("Perimetro: ");
		str.append(calcularPerimetro());
		str.append("\nSuperficie");
		str.append(calcularSuperficie());
		return str.toString();
	}
}
