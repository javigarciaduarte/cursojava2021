package modulo8.modelo;

import java.util.Objects;

public class Cuadrado extends Figura {

	private float lado;
	
	public Cuadrado() {
		// TODO Auto-generated constructor stub
	}

	public Cuadrado(String nombre, float lado) {
		super(nombre);
		this.lado = lado;
	}

	public float getLado() {
		return lado;
	}

	public void setLado(float lado) {
		this.lado = lado;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(lado);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuadrado other = (Cuadrado) obj;
		return Float.floatToIntBits(lado) == Float.floatToIntBits(other.lado);
	}

	@Override
	public String toString() {
		return "Cuadrado [lado=" + lado + super.toString() + "]";
	}
	
	@Override
	public float calcularPerimetro() {
		return lado * 4;
	}

	@Override
	public float calcularSuperficie() {
		float superficie = (float) (Math.pow(lado, 2));
		
		if (superficie > Figura.maximaSuperficie) {
			Figura.maximaSuperficie = superficie;
		}
		
		return superficie;
	}

	@Override
	public String getValores() {
		return super.getValores();
	}
}
