package modulo8.test;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import modulo8.modelo.Circulo;
import modulo8.modelo.Cuadrado;
import modulo8.modelo.Figura;
import modulo8.modelo.Rectangulo;
import modulo8.modelo.Triangulo;

public class FiguraTest {
	Figura circulo1;
	Figura circulo2;
	Figura cuadrado1;
	Figura cuadrado2;
	Figura rectangulo1;
	Figura rectangulo2;
	Figura triangulo1;
	Figura triangulo2;

	List<Figura> lFiguras;
	Set<Figura> sFiguras;

	@BeforeEach
	public void setUpBeforeClass() throws Exception {
		circulo1 = new Circulo();
		circulo2 = new Circulo("Circulo2", 2.5f);
		cuadrado1 = new Cuadrado();
		cuadrado2 = new Cuadrado("Cuadrado2", 3.6f);
		rectangulo1 = new Rectangulo();
		rectangulo2 = new Rectangulo("Rectangulo2", 4.1f, 1.7f);
		triangulo1 = new Triangulo();
		triangulo2 = new Triangulo("Triangulo2", 4.5f, 3.3f, 2.8f);

		lFiguras = new ArrayList<Figura>();
		sFiguras = new HashSet<Figura>();

		lFiguras.add(circulo1);
		lFiguras.add(circulo2);
		lFiguras.add(cuadrado1);
		lFiguras.add(cuadrado2);
		lFiguras.add(rectangulo1);
		lFiguras.add(rectangulo2);
		lFiguras.add(triangulo1);
		lFiguras.add(triangulo2);

		sFiguras.add(circulo1);
		sFiguras.add(circulo2);
		sFiguras.add(cuadrado1);
		sFiguras.add(cuadrado2);
		sFiguras.add(rectangulo1);
		sFiguras.add(rectangulo2);
		sFiguras.add(triangulo1);
		sFiguras.add(triangulo2);

	}

	@AfterEach
	public void tearDownAfterClass() throws Exception {
		circulo1 = null;
		circulo2 = null;
		cuadrado1 = null;
		cuadrado2 = null;
		rectangulo1 = null;
		rectangulo2 = null;
		triangulo1 = null;
		triangulo2 = null;

		lFiguras = null;
		sFiguras = null;
	}

	//Testeo de los constructores
	@Test
	void testFigura() {
		assertEquals(null, circulo1.getNombre());
		assertEquals(null, cuadrado1.getNombre());
		assertEquals(null, rectangulo1.getNombre());
		assertEquals(null, triangulo1.getNombre());
	}

	@Test
	void testFiguraString() {
		assertEquals("Circulo2", circulo2.getNombre());
		assertEquals("Cuadrado2", cuadrado2.getNombre());
		assertEquals("Rectangulo2", rectangulo2.getNombre());
		assertEquals("Triangulo2", triangulo2.getNombre());
	}

	//Testeo del m�todo equals() cuando debe ser true
	@Test
	void testEqualsTrue() {
		Figura circulo3 = new Circulo();
		Figura cuadrado3 = new Cuadrado();
		Figura rectangulo3 = new Rectangulo();
		Figura triangulo3 = new Triangulo();

		assertTrue(circulo1.equals(circulo3));
		assertTrue(cuadrado1.equals(cuadrado3));
		assertTrue(rectangulo1.equals(rectangulo3));
		assertTrue(triangulo1.equals(triangulo3));
	}

	//Testeo del m�todo equals() cuando debe ser false
	@Test
	void testEqualsFalse() {
		assertFalse(circulo1.equals(circulo2));
		assertFalse(cuadrado1.equals(cuadrado2));
		assertFalse(rectangulo1.equals(rectangulo2));
		assertFalse(triangulo1.equals(triangulo2));
	}

	//Testeo de los m�todos getPerimetro() y getSuperficie() de la clase Circulo
	@Test
	void testGetPerimetroCirculo() {
		float perimetro = 15.707963f;

		//Cuando el objeto Circulo est� vac�o
		assertEquals(0.0f, ((Circulo) circulo1).calcularPerimetro());
		
		//Cuando el objeto Circulo no est� vac�o
		assertEquals(perimetro, ((Circulo) circulo2).calcularPerimetro());
	}

	@Test
	void testGetSuperficieCirculo() {
		float superficie = 19.634954f;

		//Cuando el objeto Circulo est� vac�o
		assertEquals(0.0f, ((Circulo) circulo1).calcularSuperficie());
		
		//Cuando el objeto Circulo no est� vac�o
		assertEquals(superficie, ((Circulo) circulo2).calcularSuperficie());
	}
	
	//Testeo de los m�todos getPerimetro() y getSuperficie() de la clase Cuadrado
	@Test
	void testGetPerimetroCuadrado() {
		float perimetro = 14.4f;

		//Cuando el objeto Cuadrado est� vac�o
		assertEquals(0.0f, ((Cuadrado) cuadrado1).calcularPerimetro());
		
		//Cuando el objeto Cuadrado no est� vac�o
		assertEquals(perimetro, ((Cuadrado) cuadrado2).calcularPerimetro());
	}
	
	@Test
	void testGetSuperficieCuadrado() {
		float superficie = 12.959999f;
		
		//Cuando el objeto Cuadrado est� vac�o
		assertEquals(0.0f, ((Cuadrado) cuadrado1).calcularSuperficie());
		
		//Cuando el objeto Cuadrado no est� vac�o
		assertEquals(superficie, ((Cuadrado) cuadrado2).calcularSuperficie());
	}

	//Testeo de los m�todos getPerimetro() y getSuperficie() de la clase Rectangulo
	@Test
	void testGetPerimetroRectangulo() {
		float perimetro = 11.6f;

		//Cuando el objeto Rectangulo est� vac�o
		assertEquals(0.0f, ((Rectangulo) rectangulo1).calcularPerimetro());
		
		//Cuando el objeto Rectangulo no est� vac�o
		assertEquals(perimetro, ((Rectangulo) rectangulo2).calcularPerimetro());
	}
	
	@Test
	void testGetSuperficieRectangulo() {
		float superficie = 6.9700003f;
	
		//Cuando el objeto Rectangulo est� vac�o
		assertEquals(0.0f, ((Rectangulo) rectangulo1).calcularSuperficie());
		
		//Cuando el objeto Rectangulo no est� vac�o
		assertEquals(superficie, ((Rectangulo) rectangulo2).calcularSuperficie());
	}

	//Testeo de los m�todos getPerimetro() y getSuperficie() de la clase Triangulo
	@Test
	void testGetPerimetroTriangulo() {
		float perimetro = 10.6f;

		//Cuando el objeto Triangulo est� vac�o
		assertEquals(0.0f, ((Triangulo) triangulo1).calcularPerimetro());
		
		//Cuando el objeto Triangulo no est� vac�o
		assertEquals(perimetro, ((Triangulo) triangulo2).calcularPerimetro());
	}

	@Test
	void testGetSuperficieTriangulo() {
		float superficie = 4.6043468f;

		//Cuando el objeto Triangulo est� vac�o
		assertEquals(0.0f, ((Triangulo) triangulo1).calcularSuperficie());
		
		//Cuando el objeto Triangulo no est� vac�o
		assertEquals(superficie, ((Triangulo) triangulo2).calcularSuperficie());
	}

	//Testeo del m�todo getMaximaSuperficie() de la clase padre Figura
	@Test
	void testGetMaximaSuperficieInicio() {
		float maxS = 19.634954f;
		
		/*
		 * C�lculo de todas las superficies
		 * Revisar m�todo calcularSuperficie() de las clases hijo
		 */
		for (int i = 0; i < lFiguras.size(); i++) {
			lFiguras.get(i).calcularSuperficie();
		}
		
		assertEquals(maxS, Figura.getMaximaSuperficie());
	}
	
	/*
	 * Testeo del comportamiento dentro de una lista
	 */

	//Testeo si contiene objetos vac�os
	@Test
	void testListaContainsEmptyTrue() {
		Figura circulo3 = new Circulo();
		Figura cuadrado3 = new Cuadrado();
		Figura rectangulo3 = new Rectangulo();
		Figura triangulo3 = new Triangulo();

		assertTrue(lFiguras.contains(circulo3));
		assertTrue(lFiguras.contains(cuadrado3));
		assertTrue(lFiguras.contains(rectangulo3));
		assertTrue(lFiguras.contains(triangulo3));
	}

	//Testeo si contiene objetos no vac�os
	@Test
	void testListaContainsFullTrue() {
		Figura circulo4 = new Circulo("Circulo2", 2.5f);
		Figura cuadrado4 = new Cuadrado("Cuadrado2", 3.6f);
		Figura rectangulo4 = new Rectangulo("Rectangulo2", 4.1f, 1.7f);
		Figura triangulo4 = new Triangulo("Triangulo2", 4.5f, 3.3f, 2.8f);

		assertTrue(lFiguras.contains(circulo4));
		assertTrue(lFiguras.contains(cuadrado4));
		assertTrue(lFiguras.contains(rectangulo4));
		assertTrue(lFiguras.contains(triangulo4));
	}

	//Testeo si no contiene el objeto dado
	@Test
	void testListaContainsFalse() {
		Figura circulo4 = new Circulo("Circulo5", 3.0f);
		Figura cuadrado4 = new Cuadrado("Cuadrado5", 9.5f);
		Figura rectangulo4 = new Rectangulo("Rectangulo5", 3.7f, 8.6f);
		Figura triangulo4 = new Triangulo("Triangulo5", 5.1f, 9.0f, 7.2f);

		// Terminarlo
		assertFalse(lFiguras.contains(circulo4));
		assertFalse(lFiguras.contains(cuadrado4));
		assertFalse(lFiguras.contains(rectangulo4));
		assertFalse(lFiguras.contains(triangulo4));
	}

	//Testeo si a�ade objetos vac�os
	@Test
	void testListaAddEmpty() {
		Figura circulo4 = new Circulo();
		Figura cuadrado4 = new Cuadrado();
		Figura rectangulo4 = new Rectangulo();
		Figura triangulo4 = new Triangulo();

		assertTrue(lFiguras.add(circulo4));
		assertTrue(lFiguras.add(cuadrado4));
		assertTrue(lFiguras.add(rectangulo4));
		assertTrue(lFiguras.add(triangulo4));
	}

	//Testeo si a�ade objetos no vac�os
	@Test
	void testListaAddFull() {
		Figura circulo4 = new Circulo("Circulo5", 3.0f);
		Figura cuadrado4 = new Cuadrado("Cuadrado5", 9.5f);
		Figura rectangulo4 = new Rectangulo("Rectangulo5", 3.7f, 8.6f);
		Figura triangulo4 = new Triangulo("Triangulo5", 5.1f, 9.0f, 7.2f);

		assertTrue(lFiguras.add(circulo4));
		assertTrue(lFiguras.add(cuadrado4));
		assertTrue(lFiguras.add(rectangulo4));
		assertTrue(lFiguras.add(triangulo4));
	}

	//Testeo si borra objetos vac�os y no vac�os
	@Test
	void testListaRemoveTrue() {
		assertTrue(lFiguras.remove(circulo1));
		assertTrue(lFiguras.remove(circulo2));
		assertTrue(lFiguras.remove(cuadrado1));
		assertTrue(lFiguras.remove(cuadrado2));
		assertTrue(lFiguras.remove(rectangulo1));
		assertTrue(lFiguras.remove(rectangulo2));
		assertTrue(lFiguras.remove(triangulo1));
		assertTrue(lFiguras.remove(triangulo2));
	}

	//Testeo si borra objetos que no est�n en la lista
	@Test
	void testListaRemoveFalse() {
		Figura circulo4 = new Circulo("Circulo10", 2.4f);
		Figura cuadrado4 = new Cuadrado("Cuadrado10", 9.5f);
		Figura rectangulo4 = new Rectangulo("Rectangulo10", 7.3f, 6.8f);
		Figura triangulo4 = new Triangulo("Triangulo10", 1.5f, 0.9f, 2.7f);

		assertFalse(lFiguras.remove(circulo4));
		assertFalse(lFiguras.remove(cuadrado4));
		assertFalse(lFiguras.remove(rectangulo4));
		assertFalse(lFiguras.remove(triangulo4));
	}

	//Testeo el cambio de los datos de un objeto de la clase Circulo
	@Test
	void testListaModifyCirculo() {
		Figura circulo4 = new Circulo("Circulo5", 3.0f);

		((Circulo) lFiguras.get(0)).setNombre(((Circulo) circulo4).getNombre());
		((Circulo) lFiguras.get(0)).setRadio(((Circulo) circulo4).getRadio());

		assertEquals(lFiguras.get(0), circulo4);
	}

	//Testeo el cambio de los datos de un objeto de la clase Cuadrado
	@Test
	void testListaModifyCuadrado() {
		Figura cuadrado4 = new Cuadrado("Cuadrado5", 9.5f);

		((Cuadrado) lFiguras.get(2)).setNombre(((Cuadrado) cuadrado4).getNombre());
		((Cuadrado) lFiguras.get(2)).setLado(((Cuadrado) cuadrado4).getLado());

		assertEquals(lFiguras.get(2), cuadrado4);
	}

	//Testeo el cambio de los datos de un objeto de la clase Rectangulo
	@Test
	void testListaModifyRectangulo() {
		Figura rectangulo4 = new Rectangulo("Rectangulo5", 3.7f, 8.6f);

		((Rectangulo) lFiguras.get(4)).setNombre(((Rectangulo) rectangulo4).getNombre());
		((Rectangulo) lFiguras.get(4)).setAltura(((Rectangulo) rectangulo4).getAltura());
		((Rectangulo) lFiguras.get(4)).setBase(((Rectangulo) rectangulo4).getBase());

		assertEquals(lFiguras.get(4), rectangulo4);
	}

	//Testeo el cambio de los datos de un objeto de la clase Triangulo
	@Test
	void testListaModifyTriangulo() {
		Figura triangulo4 = new Triangulo("Triangulo5", 5.1f, 9.0f, 7.2f);

		((Triangulo) lFiguras.get(6)).setNombre(((Triangulo) triangulo4).getNombre());
		((Triangulo) lFiguras.get(6)).setLado1(((Triangulo) triangulo4).getLado1());
		((Triangulo) lFiguras.get(6)).setLado2(((Triangulo) triangulo4).getLado2());
		((Triangulo) lFiguras.get(6)).setLado3(((Triangulo) triangulo4).getLado3());

		assertEquals(lFiguras.get(6), triangulo4);
	}

	@Test
	void testSetContainsEmptyTrue() {
		Figura circulo3 = new Circulo();
		Figura cuadrado3 = new Cuadrado();
		Figura rectangulo3 = new Rectangulo();
		Figura triangulo3 = new Triangulo();

		assertTrue(sFiguras.contains(circulo3));
		assertTrue(sFiguras.contains(cuadrado3));
		assertTrue(sFiguras.contains(rectangulo3));
		assertTrue(sFiguras.contains(triangulo3));
	}

	//Testeo si contiene objetos no vac�os
	@Test
	void testSetContainsFullTrue() {
		Figura circulo4 = new Circulo("Circulo2", 2.5f);
		Figura cuadrado4 = new Cuadrado("Cuadrado2", 3.6f);
		Figura rectangulo4 = new Rectangulo("Rectangulo2", 4.1f, 1.7f);
		Figura triangulo4 = new Triangulo("Triangulo2", 4.5f, 3.3f, 2.8f);

		assertTrue(sFiguras.contains(circulo4));
		assertTrue(sFiguras.contains(cuadrado4));
		assertTrue(sFiguras.contains(rectangulo4));
		assertTrue(sFiguras.contains(triangulo4));
	}

	//Testeo si no contiene el objeto dado
	@Test
	void testSetContainsFalse() {
		Figura circulo4 = new Circulo("Circulo5", 3.0f);
		Figura cuadrado4 = new Cuadrado("Cuadrado5", 9.5f);
		Figura rectangulo4 = new Rectangulo("Rectangulo5", 3.7f, 8.6f);
		Figura triangulo4 = new Triangulo("Triangulo5", 5.1f, 9.0f, 7.2f);

		assertFalse(sFiguras.contains(circulo4));
		assertFalse(sFiguras.contains(cuadrado4));
		assertFalse(sFiguras.contains(rectangulo4));
		assertFalse(sFiguras.contains(triangulo4));
	}

	//Testeo si a�ade objetos vac�os
	@Test
	void testSetAddEmpty() {
		Figura circulo4 = new Circulo();
		Figura cuadrado4 = new Cuadrado();
		Figura rectangulo4 = new Rectangulo();
		Figura triangulo4 = new Triangulo();

		assertFalse(sFiguras.add(circulo4));
		assertFalse(sFiguras.add(cuadrado4));
		assertFalse(sFiguras.add(rectangulo4));
		assertFalse(sFiguras.add(triangulo4));
	}

	//Testeo si a�ade objetos no vac�os
	@Test
	void testSetAddFull() {
		Figura circulo4 = new Circulo("Circulo5", 3.0f);
		Figura cuadrado4 = new Cuadrado("Cuadrado5", 9.5f);
		Figura rectangulo4 = new Rectangulo("Rectangulo5", 3.7f, 8.6f);
		Figura triangulo4 = new Triangulo("Triangulo5", 5.1f, 9.0f, 7.2f);

		assertTrue(sFiguras.add(circulo4));
		assertTrue(sFiguras.add(cuadrado4));
		assertTrue(sFiguras.add(rectangulo4));
		assertTrue(sFiguras.add(triangulo4));
	}

	//Testeo si borra objetos vac�os y no vac�os
	@Test
	void testSetRemoveTrue() {
		assertTrue(sFiguras.remove(circulo1));
		assertTrue(sFiguras.remove(circulo2));
		assertTrue(sFiguras.remove(cuadrado1));
		assertTrue(sFiguras.remove(cuadrado2));
		assertTrue(sFiguras.remove(rectangulo1));
		assertTrue(sFiguras.remove(rectangulo2));
		assertTrue(sFiguras.remove(triangulo1));
		assertTrue(sFiguras.remove(triangulo2));
	}

	//Testeo si borra objetos que no est�n en la lista
	@Test
	void testSetRemoveFalse() {
		Figura circulo4 = new Circulo("Circulo10", 2.4f);
		Figura cuadrado4 = new Cuadrado("Cuadrado10", 9.5f);
		Figura rectangulo4 = new Rectangulo("Rectangulo10", 7.3f, 6.8f);
		Figura triangulo4 = new Triangulo("Triangulo10", 1.5f, 0.9f, 2.7f);

		assertFalse(sFiguras.remove(circulo4));
		assertFalse(sFiguras.remove(cuadrado4));
		assertFalse(sFiguras.remove(rectangulo4));
		assertFalse(sFiguras.remove(triangulo4));
	}
}
