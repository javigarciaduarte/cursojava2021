package modulo7;

public class Profesor extends Persona {
	private String iosfa;
	
	public Profesor() {

	}

	public Profesor(String iosfa) {
		this.iosfa = iosfa;
	}

	public Profesor(String nombre, String apellido, String iosfa) {
		super(nombre, apellido);
		this.iosfa = iosfa;
	}

	public String getIosfa() {
		return iosfa;
	}

	public void setIosfa(String iosfa) {
		this.iosfa = iosfa;
	}

	@Override
	public String toString() {
		return "Profesor [" + super.toString() + "iosfa=" + iosfa + "]";
	}

	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
