package modulo7;

public class Prueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Alumno alumno1;
		Alumno alumno2;
		Alumno alumno3;
		Persona profesor1;
		Persona profesor2;
		Persona profesor3;
		
		alumno1 = new Alumno();
		alumno2 = new Alumno("Javier", "Garc�a-Duarte", 1);
		alumno3 = new Alumno();
		profesor1 = new Profesor();
		profesor2 = new Profesor("Gabriel", "Casas", "1A");
		profesor3 = new Profesor();
		
		System.out.println(alumno1.equals(alumno2));
		System.out.println(profesor1.equals(profesor2));
	}

}
