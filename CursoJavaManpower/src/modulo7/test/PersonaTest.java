package modulo7.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modulo7.Alumno;
import modulo7.Persona;
import modulo7.Profesor;

public class PersonaTest {
	Persona alumno1;
	Persona profesor1;
	Persona alumno2;
	Persona profesor2;
	
	List<Persona> lPersonas;
	Set<Persona> sPersonas;
	
	@Before
	public void setUp() throws Exception{
		alumno1 = new Alumno();
		alumno2 = new Alumno("Javier", "Garc�a-Duarte", 1);
		profesor1 = new Profesor();
		profesor2 = new Profesor("Gabriel", "Casas", "1A");
		
		lPersonas = new ArrayList<Persona>();
		sPersonas = new HashSet<Persona>();
		
		lPersonas.add(alumno1);
		lPersonas.add(alumno2);
		lPersonas.add(profesor1);
		lPersonas.add(profesor2);
		
		sPersonas.add(alumno1);
		sPersonas.add(alumno2);
		sPersonas.add(profesor1);
		sPersonas.add(profesor2);
	}
	
	@After
	public void tearDown() throws Exception{
		alumno1 = null;
		alumno2 = null;
		profesor1 = null;
		profesor2 = null;
	}
	
	@Test
	public void testAlumnoEqualsV() {
		Persona alumno = new Alumno();
		assertTrue(alumno1.equals(alumno));
	}
	
	@Test
	public void testAlumnoEqualsF() {
		Persona alumno = new Alumno();
		assertFalse(alumno2.equals(alumno));
	}
	
	@Test
	public void testProfesorEqualsV() {
		Persona profesor = new Profesor();
		assertTrue(profesor1.equals(profesor));
	}
	
	@Test
	public void testProfesorEqualsF() {
		Persona profesor = new Profesor();
		assertFalse(profesor2.equals(profesor));
	}
	
	@Test
	public void testPersona() {
		assertEquals(null, alumno1.getNombre());
		assertEquals(null, alumno1.getApellido());
	}
	
	@Test
	public void testPersona2() {
		assertEquals("Javier", alumno2.getNombre());
		assertEquals("Garc�a-Duarte", alumno2.getApellido());
	}
	
	@Test
	public void testListaPersonasV() {
		Persona p = new Alumno();
		assertTrue(lPersonas.contains(p));
	}
	
	@Test
	public void testListaPersonasF() {
		Persona p = new Alumno("Luc�a", "Garc�a-Duarte", 2);
		assertFalse(lPersonas.contains(p));
	}
	
	@Test
	public void testListaPersonasAdd() {
		Persona p = new Alumno();
		assertTrue(lPersonas.add(p));
	}
	
	@Test
	public void testListaPersonasQuit() {
		Persona p = new Alumno();
		assertTrue(lPersonas.remove(p));
	}
	
	//TODO no funciona testSetPersonasV
	@Test
	public void testSetPersonasV() {
		Persona p = new Alumno();
		assertTrue(sPersonas.contains(p));
	}
	
	@Test
	public void testSetPersonasF() {
		Persona p = new Profesor("Luc�a", "Garc�a-Duarte", "2B");
		assertFalse(sPersonas.contains(p));
	}
	
	@Test
	public void testSetPersonasAdd() {
		Persona p = new Alumno();
		assertTrue(sPersonas.add(p));
	}
	
	//TODO no funciona testSetPersonasQuit
	@Test
	public void testSetPersonasQuit() {
		Persona p = new Alumno();
		assertTrue(sPersonas.remove(p));
	}
}
