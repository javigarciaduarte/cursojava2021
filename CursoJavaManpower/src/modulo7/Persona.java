package modulo7;

public class Persona {
	private String nombre;
	private String apellido;
	
	public Persona() {
		
	}
	
	public Persona(String nombre, String apellido) {
		this.nombre = nombre;
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellido=" + apellido + "]";
	}

	public boolean equals(Object arg0) {
		boolean result = false;
		
		if(arg0 instanceof Persona) {
			Persona p = (Persona) arg0;
			result = this.nombre == p.getNombre() && this.apellido == p.getApellido();
		}
		
		return result;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
