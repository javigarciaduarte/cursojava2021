package modulo7;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.ChangeEvent;

public class Ejercicio2 {

	private JFrame frame;
	private JTextField txt_nombre;
	private JTextField txt_apellido;
	private JTextField txt_legajo;
	private JTextField txt_iosfa;
	private JTable tabla_personas;
	private Persona p, pModifyDelete;
	private List<Persona> listado;
	private String[][] arrayPersonas;
	private DefaultTableModel tm;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		/*
		 * Se ha intentado pero no se ha conseguido, si encuentro algo de tiempo lo termino
		 */
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio2 window = new Ejercicio2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		String col[] = {"Nombre", "Apellido"};
		tm = new DefaultTableModel(col, 0);
		listado = new ArrayList<Persona>();
		p = new Persona();

		frame = new JFrame();
		frame.setBounds(100, 100, 636, 603);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel title = new JLabel("Personas");
		title.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 30));
		title.setBounds(215, 34, 139, 37);
		frame.getContentPane().add(title);

		JLabel lbl_nombre = new JLabel("Nombre");
		lbl_nombre.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl_nombre.setBounds(60, 159, 80, 25);
		frame.getContentPane().add(lbl_nombre);

		txt_nombre = new JTextField();
		txt_nombre.setBounds(190, 159, 223, 26);
		frame.getContentPane().add(txt_nombre);
		txt_nombre.setColumns(10);

		JRadioButton rb_alumno = new JRadioButton("Alumno");
		rb_alumno.setFont(new Font("Tahoma", Font.PLAIN, 20));
		rb_alumno.setBounds(464, 159, 93, 33);
		frame.getContentPane().add(rb_alumno);

		JRadioButton rb_profesor = new JRadioButton("Profesor");
		rb_profesor.setFont(new Font("Tahoma", Font.PLAIN, 20));
		rb_profesor.setBounds(464, 209, 99, 33);
		frame.getContentPane().add(rb_profesor);

		txt_apellido = new JTextField();
		txt_apellido.setColumns(10);
		txt_apellido.setBounds(190, 216, 223, 26);
		frame.getContentPane().add(txt_apellido);

		JLabel lbl_apellido = new JLabel("Apellido");
		lbl_apellido.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl_apellido.setBounds(60, 216, 82, 25);
		frame.getContentPane().add(lbl_apellido);

		JLabel lbl_legajo = new JLabel("Legajo");
		lbl_legajo.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl_legajo.setBounds(60, 275, 82, 25);
		frame.getContentPane().add(lbl_legajo);

		txt_legajo = new JTextField();
		txt_legajo.setColumns(10);
		txt_legajo.setBounds(190, 275, 82, 26);
		frame.getContentPane().add(txt_legajo);

		txt_iosfa = new JTextField();
		txt_iosfa.setColumns(10);
		txt_iosfa.setBounds(449, 274, 82, 26);
		frame.getContentPane().add(txt_iosfa);

		JLabel lbl_iosfa = new JLabel("IOSFA");
		lbl_iosfa.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl_iosfa.setBounds(319, 274, 82, 25);
		frame.getContentPane().add(lbl_iosfa);

		tabla_personas = new JTable();
		tabla_personas.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Nombre", "Apellido"
			}
		));
		tabla_personas.setBounds(60, 415, 497, 130);
		frame.getContentPane().add(tabla_personas);

		JButton btn_agregar = new JButton("Agregar");
		btn_agregar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btn_agregar.setBounds(60, 331, 113, 33);
		frame.getContentPane().add(btn_agregar);

		JButton btn_modificar = new JButton("Modificar");
		btn_modificar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btn_modificar.setBounds(190, 331, 113, 33);
		frame.getContentPane().add(btn_modificar);

		JButton btn_eliminar = new JButton("Eliminar");
		btn_eliminar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btn_eliminar.setBounds(319, 331, 113, 33);
		frame.getContentPane().add(btn_eliminar);

		JButton btn_buscar = new JButton("Buscar");
		btn_buscar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btn_buscar.setBounds(444, 331, 113, 33);
		frame.getContentPane().add(btn_buscar);

		JButton btn_limpiar = new JButton("Limpiar Campos");
		btn_limpiar.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btn_limpiar.setBounds(229, 374, 161, 31);
		frame.getContentPane().add(btn_limpiar);

		ButtonGroup grupo_rb = new ButtonGroup();
		grupo_rb.add(rb_profesor);
		grupo_rb.add(rb_alumno);

		rb_profesor.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rb_profesor.isSelected() == true) {
					txt_legajo.setVisible(false);
					lbl_legajo.setVisible(false);
					txt_iosfa.setVisible(true);
					lbl_iosfa.setVisible(true);
				}
			}
		});

		rb_alumno.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (rb_alumno.isSelected() == true) {
					txt_legajo.setVisible(true);
					lbl_legajo.setVisible(true);
					txt_iosfa.setVisible(false);
					lbl_iosfa.setVisible(false);
				}
			}
		});

		btn_agregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rb_alumno.isSelected()) {
					p = new Alumno(txt_nombre.getText().toString(), txt_apellido.getText().toString(),
							Integer.valueOf(txt_legajo.getText()));

					listado.add(p);

					JOptionPane.showMessageDialog(null, "Agregado con �xito");
					llenarTabla(listado);
				}
			}
		});

		btn_modificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});

		btn_eliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rb_alumno.isSelected()) {
					pModifyDelete = new Alumno(txt_nombre.getText().toString(), txt_apellido.getText().toString(), Integer.valueOf(txt_legajo.getText()));
				}
				
				for (int i = 0; i < listado.size(); i++) {
					JOptionPane.showMessageDialog(null, listado.get(i).toString() + "\n" + pModifyDelete.toString());

					if (listado.get(i).equals(pModifyDelete)) {
						listado.remove(i);

						JOptionPane.showMessageDialog(null, "Eliminado con �xito");
					}
				}
			}
		});

		btn_buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});

		btn_limpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
	}
	
	public void llenarTabla(List<Persona> lista) {
		arrayPersonas = new String[lista.size()][2];
		System.out.println(lista.size());
		
		for (int i = 0; i < lista.size(); i++) {
			arrayPersonas[i][0] = lista.get(i).getNombre().toString();
			arrayPersonas[i][1] = lista.get(i).getApellido().toString();
			
			JOptionPane.showMessageDialog(null, arrayPersonas[i][0] + " " + arrayPersonas[i][1]);
		}
		
		for (int i = 0; i < tabla_personas.getRowCount(); i++) {
			tm.removeRow(i);
		}
		
		tm.addRow(arrayPersonas);
		
		tabla_personas.setModel(tm);
	}
}
