package modulo7;

public class Alumno extends Persona {
	private int legajo;
	
	public Alumno() {
		
	}
	
	public Alumno(int legajo) {
		this.legajo = legajo;
	}

	public Alumno(String nombre, String apellido, int legajo) {
		super(nombre, apellido);
		this.legajo = legajo;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	
	
	@Override
	public String toString() {
		return "Alumno [" + super.toString() + "legajo=" + legajo + "]";
	}

	@Override
	public boolean equals(Object arg0) {
		return super.equals(arg0);
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
