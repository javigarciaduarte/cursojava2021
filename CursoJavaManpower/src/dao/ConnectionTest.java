package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import es.com.manpower.notas.util.ConnectionManager;

public class ConnectionTest {
	
	private static ConnectionManager connection;
	
	public ConnectionTest() {
		
	}

	public static void main(String[] args) {
		try {
			//Levantar el driver
			Class.forName("com.mysql.jdbc.Driver");
			
			//Conexi�n
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/manpower?characterEncoding=utf8", "root", "");
			
			//Query
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery("SELECT alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit FROM alumnos");
			
			while (rs.next()) {
				System.out.println(rs.getInt("alu_id"));
				System.out.print(rs.getString("alu_nombre") + " ");
				System.out.println(rs.getString("alu_apellido"));
				System.out.println(rs.getString("alu_estudios"));
				System.out.println(rs.getString("alu_linkgit") + "\n");
			}
			
			rs.close();
			
			con.close();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

}
